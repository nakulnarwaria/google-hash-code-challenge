import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;


public class FirstChallenge {

    public static int maximizePieceSize(int startX, int startY, int endX, int endY, int[][] tomatoes, int[][] mushroom, int l, int h, List<List<Integer>> slices, HashMap<Integer, HashMap<Integer, Integer>> memo, HashMap<Integer, HashMap<Integer, List<List<Integer>>>> sliceMemo) {


        if(endX >= tomatoes.length || endY >= tomatoes[0].length)
            return 0;

        int pieceSize = (endX-startX + 1 )* (endY- startY + 1);
        boolean isValidSliceOfPizza = isValidSlice(tomatoes, mushroom, startX, startY, endX, endY, l, h);

        //remaining pizza can be put into a slice
        if(isValidSliceOfPizza)
        {

            ArrayList<Integer> newSlice = new ArrayList<Integer>();
            newSlice.add(0,startX);
            newSlice.add(1,startY);
            newSlice.add(2,endX);
            newSlice.add(3,endY);
            slices.add(newSlice);

            // Adding memo
            if(memo.get(key(startX,startY, tomatoes[0].length))==null)
                memo.put(key(startX,startY, tomatoes[0].length), new HashMap<Integer,Integer>());
            memo.get(key(startX,startY, tomatoes[0].length)).put(key(endX,  endY, tomatoes[0].length ), pieceSize);

            if(sliceMemo.get(key(startX,startY, tomatoes[0].length))==null)
                sliceMemo.put(key(startX,startY, tomatoes[0].length), new HashMap<Integer,List<List<Integer>>>());
            sliceMemo.get(key(startX,startY, tomatoes[0].length)).put(key(endX,  endY, tomatoes[0].length ), new ArrayList<List<Integer>>(slices));

            return pieceSize;
        }

        //Pizza too small to divide, and wasted.
        if(pieceCount(startX, startY, endX, endY, tomatoes) < l || pieceCount(startX, startY, endX, endY, mushroom) < l )
            return 0;


        int max = 0;
        List<List<Integer>> slicesOfRemainingPizza = new ArrayList<List<Integer>>();
        ArrayList<List<Integer>> newSlices;

        for(int row=startX; row<= endX; row++ ) {
            for(int col = startY; col<= endY; col++ ) {

                //Too big slice to consider
                pieceSize = (row-startX + 1 )* (col- startY + 1);
                if(pieceSize > h) {
                    break;
                }

                int maxOfCurrentSlice = 0;
                newSlices = new ArrayList<List<Integer>>();


                //if slice is considerations is a valid slice, and not waste
                if(isValidSlice(tomatoes, mushroom, startX, startY, row, col, l, h)) {
                    maxOfCurrentSlice += pieceSize;

                    ArrayList<Integer> newSlice = new ArrayList<Integer>();
                    newSlice.add(0,startX);
                    newSlice.add(1,startY);
                    newSlice.add(2,row);
                    newSlice.add(3,col);
                    newSlices.add(newSlice);

                }

                if(memo.get(key(startX,col+1,tomatoes[0].length))!=null && memo.get(key(startX,col+1,tomatoes[0].length)).get(key(row, endY, tomatoes[0].length)) !=null ) {
                    maxOfCurrentSlice += memo.get(key(startX,col+1,tomatoes[0].length)).get(key(row, endY, tomatoes[0].length));
                    newSlices.addAll(sliceMemo.get(key(startX,col+1,tomatoes[0].length)).get(key(row, endY, tomatoes[0].length)));
                }
                else {
                    maxOfCurrentSlice += maximizePieceSize(startX, col+1, row, endY, tomatoes, mushroom, l, h, newSlices, memo, sliceMemo );
                }

                if(memo.get(key(row+1, startY,tomatoes[0].length))!=null && memo.get(key(row+1, startY,tomatoes[0].length)).get(key(endX, endY, tomatoes[0].length)) !=null ) {
                    maxOfCurrentSlice += memo.get(key(row+1, startY,tomatoes[0].length)).get(key(endX, endY, tomatoes[0].length));
                    newSlices.addAll(sliceMemo.get(key(row+1, startY,tomatoes[0].length)).get(key(endX, endY, tomatoes[0].length)));
                }
                else {
                    maxOfCurrentSlice += maximizePieceSize(row+1, startY, endX, endY, tomatoes, mushroom, l, h, newSlices, memo, sliceMemo);
                }

                if(max < maxOfCurrentSlice)
                {
                    slicesOfRemainingPizza = newSlices;
                    max = maxOfCurrentSlice;
                }
                // Choosing solution with less number of slices among two equal score solutions
                else if(max == maxOfCurrentSlice && slicesOfRemainingPizza.size() > newSlices.size()) {
                    slicesOfRemainingPizza = newSlices;
                    max = maxOfCurrentSlice;
                }

            }
        }

        slices.addAll(slicesOfRemainingPizza);

        // Adding memo
        if(memo.get(key(startX,startY, tomatoes[0].length))==null)
            memo.put(key(startX,startY, tomatoes[0].length), new HashMap<Integer,Integer>());
        memo.get(key(startX,startY, tomatoes[0].length)).put(key(endX,  endY, tomatoes[0].length ), max);

        if(sliceMemo.get(key(startX,startY, tomatoes[0].length))==null)
            sliceMemo.put(key(startX,startY, tomatoes[0].length), new HashMap<Integer,List<List<Integer>>>());
        sliceMemo.get(key(startX,startY, tomatoes[0].length)).put(key(endX,  endY, tomatoes[0].length ), new ArrayList<List<Integer>>(slicesOfRemainingPizza));

        return max;

    }

    public static int pieceCount(int r1, int c1, int r2, int c2, int[][] matrix) {

        if(r1-1 <0 && c1-1 <0)
            return matrix[r2][c2];

        else if(c1-1 < 0)
            return matrix[r2][c2] - matrix[r1-1][c2];

        else if(r1-1 < 0)
            return matrix[r2][c2] - matrix[r2][c1-1];

        else
            return matrix[r2][c2] - matrix[r2][c1-1] - matrix[r1-1][c2] + matrix[r1-1][c1-1];
    }



    public static void makePieceCountMatrix(char[][] pizza, int[][] mushroom, int[][] tomato ) {

        int rowTomatoCount  = 0;
        int rowMushroomCount = 0;
        for(int row=0;row<pizza.length;row++) {
            for(int col= 0; col<pizza[0].length; col++) {
                if(pizza[row][col] == 'T')
                    rowTomatoCount++;
                else
                    rowMushroomCount++;
                if(row == 0) {
                    tomato[row][col] = rowTomatoCount;
                    mushroom[row][col] = rowMushroomCount;
                }

                else {
                    tomato[row][col] = tomato[row-1][col] + rowTomatoCount;
                    mushroom[row][col] = mushroom[row-1][col] + rowMushroomCount;
                }

            }
            rowTomatoCount  = 0;
            rowMushroomCount = 0;

        }

    }


    public static boolean isValidSlice(int[][] tomatoes, int[][] mushrooms, int r1, int c1, int r2, int c2, int l, int h) {

        if(r1 < 0 || c1< 0 || r2>=tomatoes.length || c2 >= tomatoes[0].length)
            return false;

        if(r1> r2 || c1 > c2)
            return false;

        int totalTomatoes = pieceCount(r1,c1,r2,c2,tomatoes);
        int totalMushrooms = pieceCount(r1,c1,r2,c2,mushrooms);

        if(totalTomatoes < l)
            return false;
        if(totalMushrooms < l)
            return false;
        if(totalTomatoes + totalMushrooms > h)
            return false;

        return true;

    }

    public static int key(int row, int col, int numCol) {
        if(row == 0)
            return col+1;
        else
            return (row*(numCol)) + col+1;
    }

    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter input file address - ");
        String inputPath = sc.next();
		solution(inputPath);

    }

    public static void solution(String fileName) throws IOException {

        FileInputStream fstream = new FileInputStream(fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String strLine  = br.readLine();
        String[] inputs = strLine.split(" ");

        int row = Integer.parseInt(inputs[0]);
        int col = Integer.parseInt(inputs[1]);
        int l = Integer.parseInt(inputs[2]);
        int h = Integer.parseInt(inputs[3]);
        int r=0;
        char[][] pizza = new char[row][];

        //Read File Line By Line
        while ((strLine = br.readLine()) != null)   {
            // Print the content on the console
            pizza[r] = strLine.toCharArray();
            r++;
        }

        //Close the input stream
        fstream.close();

        //Close the input stream
        fstream.close();

        int[][] tomatoCount = new int[row][col];
        int[][] mushroomCount = new int[row][col];
        makePieceCountMatrix(pizza,mushroomCount, tomatoCount);

        HashMap<Integer, HashMap<Integer,Integer>> memo = new HashMap<>();
        HashMap<Integer, HashMap<Integer, List<List<Integer>>>> sliceMemo = new HashMap<>();

        ArrayList<List<Integer>> slices = new ArrayList<List<Integer>>();

        maximizePieceSize(0,0, row-1, col-1, tomatoCount, mushroomCount, l,h, slices, memo, sliceMemo);

        List<String> output = new ArrayList<String>();
        output.add(0,""+slices.size());
        int i=1;
        while(i<=slices.size()) {
            String out = "";
            for(int j=0;j<slices.get(i-1).size();j++)
                out += slices.get(i-1).get(j)+" ";
            System.out.println(out);
            output.add(i, out);
            i++;
        }
        Path file = Paths.get("output.txt");
        Files.write(file, output, Charset.forName("UTF-8"));

    }


}


